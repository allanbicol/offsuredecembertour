<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Duos - Multipurpose HTML5 Landing Page</title>
<meta name="description" content="Startup landing page">
<meta name="author" content="Multifour | multifour.com">

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->
<link rel="stylesheet" href="{{ URL::asset('template-assets/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{ URL::asset('template-assets/css/style.css')}}"/>
<link rel="stylesheet" href="{{ URL::asset('template-assets/css/iconfont-style.css')}}"/>

<!-- Favicons -->
<link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
<style type="text/css">
    .panel-title {
    display: inline;
    font-weight: bold;
    }
    .display-table {
        /* display: table; */
        text-align: justify;
    }
    .display-tr {
        display: table-row;
    }
    .display-td {
        display: table-cell;
        vertical-align: middle;
        width: 68%;
    }
</style>
</head>

<body>
<div id="wrap">
	<div class="container-fluid">
		<div class="row">

			<!-- INTRO BEGIN -->
			<div id="intro" class="col-lg-6"> <a href="#" class="logo"> <img src="{{ URL::asset('template-assets/images/studytour.png')}}" alt="Duos - Multipurpose HTML5 Landing Page" width="250"/> </a>

				<!-- NAVIGATION BEGIN -->
				{{-- <nav class="navbar"> <a class="menu-btn collapsed" data-toggle="collapse" href="#menu-list"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
					<div id="menu-list" class="panel-collapse collapse">
						<ul class="nav">
							<li><a href="about.html">About</a> </li>
							<li><a href="contact.html">Contact</a></li>
							<li><a href="http://themeforest.net/item/duos-multipurpose-html5-landing-page/9532968?ref=multifour" target="_blank">Sign In</a></li>
						</ul>
					</div>
				</nav> --}}
				<!-- NAVIGAION END -->

				<div id="slides">
					<ul class="slides-container_" >
						<li class="orange-bg">
							<h2 class="slogan"> <img src="{{ URL::asset('template-assets/images/rebiz.png')}}" style="opacity:1;" width="60%"/><small style="font-size:15px;">2020, what's your Real Estate business in for? Do you have a goal in mind and more importantly, a strategy in place? Can your current team take your business there? Put your mind at ease by joining us for two days of learning focusing on how to leverage a Property Management team consisting of both in-house and remote employees. Find out what makes outsourcing a viable business strategy for Real Estate from experienced clients and employees. You don’t have to take our word for it - click here to watch our interviews with several Real Estate owners in both Australia and New Zealand.
                            </small> </h2>
							<img src="{{ URL::asset('template-assets/images/1.jpg')}}" alt=""> </li>
						{{-- <li class="blue-bg">
							<h2 class="slogan"> Simple &amp; Pure <small>No fluff. Nothing should lead the visitor away from the main essence of website. There must be just important information.</small> </h2>
							<img src="images/2.jpg" alt=""> </li>
						<li class="purple-bg">
							<h2 class="slogan"> Innovations <small>In our work we try to use only the most modern, convenient and interesting solutions.
								We want the template you downloaded look unique and new for such a long time as it is possible.</small> </h2>
							<img src="images/3.jpg" alt=""> </li> --}}
					</ul>
				</div>
			</div>
			<!-- INTRO END -->

			<!-- CONTENT BEGIN -->
			<div id="content" class="main-page col-lg-6 col-lg-offset-6">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" style="width:100%;text-align:left">
                                <thead>
                                    <tr>
                                        <th>Item</th><th>Price</th><th style="text-align:right">Qty</th>
                                    </tr>
                                </thead>
                            <tbody>

                                <tr>
                                    <td>Offsure Real Estate Study Tour</td>
                                    <td>AUD 990</td>
                                    <td style="text-align:right">1</td>
                                </tr>
                                <tr><td colspan="3"><div style="height:20px;"></div></td></tr>
                                <tr style="background-color:rgb(0, 62, 82);color:#fff;padding:5px">
                                        <td colspan="2" style="padding:5px;">Total</td>

                                        <td style="text-align:right;padding:5px;">AUD 990</td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
				<div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default credit-card-box">
                            <div class="panel-heading display-table" >
                                <div class="row display-tr" >
                                    <h3 class="panel-title display-td" >Payment Details</h3>
                                    <div class="display-td" >
                                        <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">

                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <p style="color:">{{ Session::get('success') }}</p>
                                    </div>
                                @endif

                                <form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
                                                                 data-cc-on-file="false"
                                                                data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                                id="payment-form">
                                    @csrf

                                    <div class='form-row row'>
                                        <div class='col-xs-12 form-group required'>
                                            <label class='control-label'>Name on Card</label> <input
                                                class='form-control' size='4' type='text'>
                                        </div>
                                    </div>

                                    <div class='form-row row'>
                                        <div class='col-xs-12 form-group card required'>
                                            <label class='control-label'>Card Number</label> <input
                                                autocomplete='off' class='form-control card-number' size='20'
                                                type='text'>
                                        </div>
                                    </div>

                                    <div class='form-row row'>
                                        <div class='col-xs-12 col-md-4 form-group cvc required'>
                                            <label class='control-label'>CVC</label> <input autocomplete='off'
                                                class='form-control card-cvc' placeholder='ex. 311' size='4'
                                                type='text'>
                                        </div>
                                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                                            <label class='control-label'>Expiration Month</label> <input
                                                class='form-control card-expiry-month' placeholder='MM' size='2'
                                                type='text'>
                                        </div>
                                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                                            <label class='control-label'>Expiration Year</label> <input
                                                class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                                type='text'>
                                        </div>
                                    </div>

                                    <div class='form-row row'>
                                        <div class='col-md-12 error form-group hide'>
                                            <div class='alert-danger alert'>Please correct the errors and try
                                                again.</div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<!-- CONTENT END -->

		</div>
	</div>
</div>



<!-- JavaScript -->
<script src="{{ URL::asset('template-assets/scripts/jquery-1.8.2.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/jquery.validate.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/smoothscroll.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/jquery.superslides.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/placeholders.jquery.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/custom.js')}}"></script>

<!--[if lte IE 9]>
	<script src="{{ URL::asset('template-assetscss/scripts/respond.min.js')}}"></script>
<![endif]-->

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');

        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });

    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }

  });

  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }

});
</script>
</body>
</html>




{{-- <!DOCTYPE html>
<html>
<head>
	<title>Laravel 5 - Stripe Payment Gateway Integration Example - ItSolutionStuff.com</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>
</head>
<body>

<div class="container">

    <h1>Laravel 5 - Stripe Payment Gateway Integration Example <br/> ItSolutionStuff.com</h1>



</div>

</body>


</html> --}}
