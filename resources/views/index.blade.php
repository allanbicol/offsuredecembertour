<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="Real Estate Study Tour">
    <meta name="author" content="Offsure">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content=""/> <!-- website name -->
    <meta property="og:site" content=""/> <!-- website link -->
    <meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content=""/> <!-- description shown in the actual shared post -->
    <meta property="og:image" content=""/> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content=""/> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article"/>

    <!--title-->
    <title>Offsure Real Estate Study Tour</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/bootstrap.min.css')}}">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/magnific-popup.css')}}">
    <!--Themify icon css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/themify-icons.css')}}">
    <!--animated css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/animate.min.css')}}">
    <!--ytplayer css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/jquery.mb.YTPlayer.min.css')}}">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.theme.default.min.css')}}">
    <!--custom css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/responsive.css')}}">

</head>
<body>

<!--header section start-->
<header class="header">
    <!--start navbar-->
    <nav class="navbar navbar-expand-lg fixed-top custom-nav white-bg" style="padding:30px;">
        <div class="container">
            <a class="navbar-brand" href="index-3.html"><img src="{{ URL::asset('new-assets/img/logo.png')}}" width="200" alt="logo"
                                                           class="img-fluid"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button>

            <!-- <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link page-scroll dropdown-toggle" href="#" id="navbarDropdownHome" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Home
                        </a>
                        <div class="dropdown-menu submenu" aria-labelledby="navbarDropdownHome">
                            <a class="dropdown-item" href="index.html">Demo Template 1</a>
                            <a class="dropdown-item" href="index-2.html">Demo Template 2</a>
                            <a class="dropdown-item" href="index-3.html">Demo Template 3</a>
                            <a class="dropdown-item" href="index-4.html">Demo Template 4</a>
                            <a class="dropdown-item" href="index-5.html">Demo Template 5</a>
                            <a class="dropdown-item" href="index-6.html">Demo Template 6</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#features">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#pricing">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#screenshots">Screenshots</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact">Contact</a>
                    </li>

                </ul>
            </div> -->
        </div>
    </nav>
    <!--end navbar-->
</header>
<!--header section end-->

<!--body content wrap start-->
<div class="main">

    <!--hero section start-->
    <section class="hero-section hero-section-3 ptb-100">
        <!--animated circle shape start-->
        <div class="circles">
            <div class="point animated-point-1"></div>
            <div class="point animated-point-2"></div>
            <div class="point animated-point-3"></div>
            <div class="point animated-point-4"></div>
            <div class="point animated-point-5"></div>
            <div class="point animated-point-6"></div>
        </div>
        <!--animated circle shape end-->
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-6">
                    <div class="hero-content-left ptb-100">
                        <h1><span style="color:rgb(0, 62, 82)">Real Estate</span><div style="margin-top:-10px">Study Tour</div></h1>
                        <div style="margin-top:-20px;color:#FF4813;font-weight: bold;">April 6 & 7, 2020 | Clark, Philippines</div>
                        <div style="color:rgb(0, 62, 82)">Exclusive to owners, brokers and principals in AU and NZ.</div>
                        <br>
                        <p class="lead">What is your Real Estate plan for 2020? If you haven’t <br>figured it out, maybe we can help.</p>
                        <br>
                        <a href="{{route('signup')}}" class="btn solid-btn page-scroll btn-attend"><span style="font-size:25px;">Get Started</span><br>Here's the full agenda.</a>
                        <a href="#contact" class="btn solid-btn page-scroll" style="margin-left:10px;"><span style="font-size:25px;">Interested</span><br>But can't attend the tour?</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="hero-animation-img">
                        <img class="img-fluid d-block m-auto animation-one" src="{{ URL::asset('new-assets/img/header-slider-img.png')}}"
                             width="470" alt="animation image">
                        <img class="img-fluid d-none d-lg-block animation-two"
                             src="{{ URL::asset('new-assets/img/header-slider-img-2.png')}}" alt="animation image" width="580">
                        <!-- <img class="img-fluid d-none d-lg-block animation-three"
                             src="img/Telemedicine_03.svg" alt="animation image" width="120">
                        <img class="img-fluid d-none d-lg-block animation-four" src="img/hero-animation-03.svg"
                             alt="animation image" width="200"> -->
                    </div>
                </div>
            </div>
        </div>

        <!--shape image start-->
        <img src="{{ URL::asset('new-assets/img/hero-bg-shape-2.png')}}" class="shape-image" alt="shape image">
        <!--shape image end-->
    </section>
    <!--hero section end-->

    <!--promo section start-->
    <!-- <section class="promo-section ptb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-8">
                    <div class="section-heading text-center mb-5">
                        <h2>Why small business owners <br><span>love AppCo?</span></h2>
                        <p class="lead">
                            Following reasons show advantages of adding AppCo to your lead pages, demos and
                            checkouts
                        </p>
                    </div>
                </div>
            </div>
            <div class="row equal">
                <div class="col-md-4 col-lg-4">
                    <div class="single-promo single-promo-hover single-promo-1 rounded text-center white-bg p-5 h-100">
                        <div class="circle-icon mb-5">
                            <span class="ti-vector text-white"></span>
                        </div>
                        <h5>Clean Design</h5>
                        <p>Increase sales by showing true dynamics of your website.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="single-promo single-promo-hover single-promo-1 rounded text-center white-bg p-5 h-100">
                        <div class="circle-icon mb-5">
                            <span class="ti-lock text-white"></span>
                        </div>
                        <h5>Secure Data</h5>
                        <p>Build your online store’s trust using Social Proof & Urgency.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="single-promo single-promo-hover single-promo-1 rounded text-center white-bg p-5 h-100">
                        <div class="circle-icon mb-5">
                            <span class="ti-eye text-white"></span>
                        </div>
                        <h5>Retina Ready</h5>
                        <p>Realize importance of social proof in customer’s purchase decision.</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--promo section end-->

    <!--overflow block start-->
    <div class="overflow-hidden">
        <!--about us section start-->
        <section id="about" class="about-us ptb-100 background-shape-img">
            <div class="container" style="z-index:3">
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-12">
                        <div class="about-content-left section-heading">
                            <h2 style="color:#fff;padding-bottom:50px;">Values</h2>
                            <div class="row" style="color:#fff;font-size: 17px;">
                                    <div class="col-md-6">
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap d-flex align-items-center mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Learn how to leverage a Property Management portfolio with onshore and offshore teams.</p>
                                                </div>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Learn how an offshore team can give you more focused and free time with your loved ones.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Learn how one property management virtual assistant can take care of more than 20 tasks using different Real Estate programs such as PropertyMe, Rest, Property Tree, Console, Ire and more.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Assess the status of your business, determine what’s working and what’s not.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                    </div>

                                    <div class="col-md-6">
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Come up with your People Plan for 2020.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Strategise your business model for capacity, sustainability, and continuity.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Meet and immerse yourself into Real Estate Outsourcing with experienced property management virtual assistants trained by Real Estate experts in Australia.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Interview potential virtual or admin assistants to join your REAL Estate team.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                    </div>



                            </div>
                            <div class="row mt-4">
                                <div class="col-lg-6 text-right">
                                    <a href="{{route('signup')}}" class="btn solid-btn page-scroll btn-second"><span style="font-size:25px;">Get Started</span><br>Here's the full agenda.</a>
                                </div>
                                <div class="col-lg-6">
                                    <a href="#contact" class="btn solid-btn page-scroll btn-second" ><span style="font-size:25px;">Interested</span><br>But can't attend the tour?</a>
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- <div class="col-md-5">
                        <div class="about-content-right d-flex justify-content-center justify-content-lg-end justify-content-md-end">
                            <img src="img/image-10.png" alt="about us" class="img-fluid">
                        </div>
                    </div> -->


                </div>
            </div>
        </section>
        <!--about us section end-->


    </div>
    <!--overflow block end-->

    <!--features section start-->
    <!-- <section id="features" class="feature-section ptb-100">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-4">
                    <div class="download-img d-flex align-bottom">
                        <img src="img/image-10.png" alt="download" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="feature-contents section-heading">
                        <h2>Quick & Easy Process with <br>
                            <span>best features</span></h2>
                        <p>Objectively deliver professional value with diverse web-readiness.
                            Collaboratively transition wireless customer service without goal-oriented catalysts for
                            change. Collaboratively.</p>

                        <div class="feature-content-wrap">
                            <ul class="nav nav-tabs feature-tab" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active h6" href="#tab6-1" data-toggle="tab">
                                        <span class="ti-palette"></span>
                                        Design
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link h6" href="#tab6-2" data-toggle="tab">
                                        <span class="ti-vector"></span>
                                        Development
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link h6" href="#tab6-3" data-toggle="tab">
                                        <span class="ti-bar-chart"></span>
                                        Marketing
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link h6" href="#tab6-4" data-toggle="tab">
                                        <span class="ti-announcement"></span>
                                        Branding
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content feature-tab-content">
                                <div class="tab-pane active" id="tab6-1">
                                    <ul class="list-unstyled">
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3">
                                                        <span class="ti-check"></span>
                                                    </div>
                                                </div>
                                                <div><p class="mb-0">Tones of SASS variables</p></div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3">
                                                        <span class="ti-check"></span>
                                                    </div>
                                                </div>
                                                <div><p class="mb-0">Create your own skin to match your brand</p></div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3">
                                                        <span class="ti-check"></span>
                                                    </div>
                                                </div>
                                                <div><p class="mb-0">Globally orchestrate tactical channels whereas bricks</p></div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3">
                                                        <span class="ti-check"></span>
                                                    </div>
                                                </div>
                                                <div><p class="mb-0">Use Gulp to prepare all assets for production</p></div>
                                            </div>
                                        </li>
                                        <li class="py-2">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <div class="badge badge-circle badge-primary mr-3">
                                                        <span class="ti-check"></span>
                                                    </div>
                                                </div>
                                                <div><p class="mb-0">Collaboratively predominate vertical manufactured</p></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="tab6-2">
                                    <div class="single-feature">
                                        <div class="d-flex align-items-center mb-2">
                                            <span class="ti-layers rounded mr-3 icon icon-color-1"></span>
                                            <h5 class="mb-0">Easy to use</h5>
                                        </div>
                                        <p>Synergistically deliver next-generation relationships whereas bleeding-edge resources. Continually pontificate stand-alone benefits whereas.</p>
                                    </div>
                                    <div class="single-feature mb-4">
                                        <div class="d-flex align-items-center mb-2">
                                            <span class="ti-alarm-clock rounded mr-3 icon icon-color-2"></span>
                                            <h5 class="mb-0">Increase conversion</h5>
                                        </div>
                                        <p>Phosfluorescently empower compelling intellectual capital and revolutionary web services. Compellingly develop cross-media.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab6-3">
                                    <div class="row">
                                        <div class="col single-feature mb-4">
                                            <div class="d-flex align-items-center mb-2">
                                                <span class="ti-bar-chart rounded mr-3 icon icon-color-2"></span>
                                                <h5 class="mb-0">Increase conversion</h5>
                                            </div>
                                            <p>Display recent conversions, build credibility and trust.</p>
                                        </div>
                                        <div class="col single-feature mb-4">
                                            <div class="d-flex align-items-center mb-2">
                                                <span class="ti-stats-up rounded mr-3 icon icon-color-3"></span>
                                                <h5 class="mb-0">Product analytics</h5>
                                            </div>
                                            <p>A top promo bar that counts down until a few discounts.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab6-4">
                                    <p>I have, to push possibilities, to show
                                        people, this is the level that things could be at. So when you get something
                                        that has the name Kanye West on it, it’s supposed to be pushing the furthest
                                        possibilities. I will be the leader of a company that ends up being worth
                                        billions of dollars, because I got the answers. I understand culture. I am the
                                        nucleus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--features section end-->

    <!--our video promo section start-->
    <section class="video-promo ptb-100 background-img"
             style="background: url('{{ URL::asset('new-assets/img/hero-bg-1.jpg')}}')no-repeat center center / cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="video-promo-content mt-4 text-center">
                            <!-- <iframe width="80%" height="415" src="https://www.youtube.com/embed/uvo9C6pG2Sw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                        <a onclick="changeVideo('uvo9C6pG2Sw')"
                           class="popup-youtube video-play-icon d-inline-block"><span class="ti-control-play"></span></a>
                        <h5 class="mt-4 text-black">Business Tour, RE/MAX Exclusive</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--our video promo section end-->

    <!--our pricing packages section start-->
    <!-- <section id="pricing" class="package-section ptb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section-heading text-center mb-5">
                        <h2>Pricing Packages</h2>
                        <p class="lead">
                            Monotonectally grow strategic process improvements vis-a-vis integrated resources.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md">
                    <div class="card text-center single-pricing-pack">
                        <div class="card-header py-5 border-0 pricing-header">
                            <div class="h1 text-center mb-0">$<span class="price font-weight-bolder">29</span></div>
                            <span class="h6 text-muted">Basic License</span>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled text-sm mb-4 pricing-feature-list">
                                <li>Push Notifications</li>
                                <li>Data Transfer</li>
                                <li>SQL Database</li>
                                <li>Search & SEO Analytics</li>
                                <li>24/7 Phone Support</li>
                                <li>2 months technical support</li>
                                <li>2+ profitable keyword</li>
                            </ul>
                            <a href="#" class="btn solid-btn mb-3" target="_blank">Purchase now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md">
                    <div class="card primary-bg text-center single-pricing-pack">
                        <div class="card-header py-5 border-0 pricing-header">
                            <div class="h1 text-white text-center mb-0">$<span
                                    class="price font-weight-bolder">149</span></div>
                            <span class="h6 text-white">Extended License</span>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled text-white text-sm mb-4 pricing-feature-list">
                                <li>Push Notifications</li>
                                <li>Data Transfer</li>
                                <li>SQL Database</li>
                                <li>Search & SEO Analytics</li>
                                <li>24/7 Phone Support</li>
                                <li>1 Year technical support</li>
                                <li>50+ profitable keyword</li>
                            </ul>
                            <a href="#" class="btn app-store-btn mb-3" target="_blank">Purchase now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md">
                    <div class="card text-center single-pricing-pack">
                        <div class="card-header py-5 border-0 pricing-header">
                            <div class="h1 text-center mb-0">$<span class="price font-weight-bolder">39</span></div>
                            <span class="h6 text-muted">Standard License</span>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled text-sm mb-4 pricing-feature-list">
                                <li>Push Notifications</li>
                                <li>Data Transfer</li>
                                <li>SQL Database</li>
                                <li>Search & SEO Analytics</li>
                                <li>24/7 Phone Support</li>
                                <li>6 months technical support</li>
                                <li>10+ profitable keyword</li>
                            </ul>
                            <a href="#" class="btn solid-btn mb-3" target="_blank">Purchase now</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="mt-5 text-center">
                <p class="mb-2">If you need custom services or Need more? <a href="#" class="color-secondary">
                    Contact us
                </a></p>

            </div>
        </div>
    </section> -->
    <!--our pricing packages section end-->

    <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="max-width:50%">
      <div class="modal-content">
        <div class="modal-body">

          <iframe id="iframeYoutube" width="100%" height="515"  src="https://youtu.be/uvo9C6pG2Sw" frameborder="0" allowfullscreen></iframe>

        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> --}}
      </div>
    </div>
  </div>





    <!--contact us section start-->
    <section id="contact" class="contact-us gray-light-bg ptb-100">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <form action="{{route('add.prospect')}}" method="POST" id="contactForm1" class="contact-us-form" novalidate="novalidate">
                            {{ csrf_field() }}
                        <h5>You're interested but the tour dates don't suit you? No problem. Just drop as your contact information and we'll connect with you.</h5>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Enter name"
                                           required="required">
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Enter email"
                                           required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" name="phone" value="" class="form-control" id="phone"
                                           placeholder="Your Phone" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                <div class="form-group">
                                    <input type="text" name="company" value="" size="40" class="form-control"
                                           id="company" placeholder="Your Company">
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                <div class="form-group">
                                    <label>Preferred date of contact</label>
                                    <input type="date" name="date" value="" size="40" class="form-control"
                                           id="date" placeholder="Your Company">
                                </div>
                            </div>

                            <div class="col-sm-6 col-12">
                                <div class="form-group">
                                    <label>Preferred time of contact (QLD Time)</label>
                                    <input type="time" name="time" value="" size="40" class="form-control"
                                            id="time" placeholder="time">
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                    <div class="form-group">
                                            <label>Preferred communication method</label>
                                            <br>
                                            <label class="checkbox-inline"><input type="checkbox" name="phone_option" value="Phone"> Phone</label>
                                            <label class="checkbox-inline"><input type="checkbox" name="email_option" value="Email"> Email</label>
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 mt-3">
                                <button type="submit" class="btn solid-btn" id="btnContactUs">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                    <p class="form-message"></p>
                </div>
            </div>
        </div>
    </section>
    <!--contact us section end-->



    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg pt-4 pb-4">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-6 col-lg-5"><p class="copyright-text pb-0 mb-0">Copyrights © 2019. All
                    rights reserved by
                    <a href="https://offsure.com">Offsure</a></p>
                </div>
            </div>
            <div class="row text-center justify-content-center">
                <img src="{{ URL::asset('new-assets/img/stripelogo.png')}}" width="200px"/>

            </div>

        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--jQuery-->
<script src="{{ URL::asset('new-assets/js/jquery-3.4.1.min.js')}}"></script>
<!--Popper js-->
<script src="{{ URL::asset('new-assets/js/popper.min.js')}}"></script>
<!--Bootstrap js-->
<script src="{{ URL::asset('new-assets/js/bootstrap.min.js')}}"></script>
<!--Magnific popup js-->
<script src="{{ URL::asset('new-assets/js/jquery.magnific-popup.min.js')}}"></script>
<!--jquery easing js-->
<script src="{{ URL::asset('new-assets/js/jquery.easing.min.js')}}"></script>
<!--jquery ytplayer js-->
<script src="{{ URL::asset('new-assets/js/jquery.mb.YTPlayer.min.js')}}"></script>
<!--wow js-->
<script src="{{ URL::asset('new-assets/js/wow.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{ URL::asset('new-assets/js/owl.carousel.min.js')}}"></script>
<!--custom js-->
<script src="{{ URL::asset('new-assets/js/scripts.js')}}"></script>
</body>
</html>
