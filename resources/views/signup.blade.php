<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="Real Estate Study Tour">
    <meta name="author" content="Offsure">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content=""/> <!-- website name -->
    <meta property="og:site" content=""/> <!-- website link -->
    <meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content=""/> <!-- description shown in the actual shared post -->
    <meta property="og:image" content=""/> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content=""/> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article"/>

    <!--title-->
    <title>Offsure Real Estate Study Tour</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    {{-- <link rel="stylesheet" href="{{ URL::asset('new-assets/css/bootstrap.min.css')}}"> --}}
    <link rel="stylesheet" href="{{ URL::asset('template-assets/css/bootstrap.min.css')}}"/>
    <!--Magnific popup css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/magnific-popup.css')}}">
    <!--Themify icon css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/themify-icons.css')}}">
    <!--animated css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/animate.min.css')}}">
    <!--ytplayer css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/jquery.mb.YTPlayer.min.css')}}">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.theme.default.min.css')}}">
    <!--custom css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/responsive.css')}}">
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            /* display: table; */
            text-align: justify;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 68%;
        }
        .animation-two{
            bottom: -810px;
            left: 70px;
        }
    </style>
</head>
<body>

<!--header section start-->
<header class="header">
    <!--start navbar-->
    {{-- <nav class="navbar navbar-expand-lg fixed-top custom-nav white-bg" style="padding:30px;">
        <div class="container"> --}}
            {{-- <a class="navbar-brand" href="index-3.html"><img src="{{ URL::asset('new-assets/img/logo.png')}}" width="200" alt="logo"
                                                           class="img-fluid"></a> --}}
            {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button> --}}

            <!-- <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link page-scroll dropdown-toggle" href="#" id="navbarDropdownHome" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Home
                        </a>
                        <div class="dropdown-menu submenu" aria-labelledby="navbarDropdownHome">
                            <a class="dropdown-item" href="index.html">Demo Template 1</a>
                            <a class="dropdown-item" href="index-2.html">Demo Template 2</a>
                            <a class="dropdown-item" href="index-3.html">Demo Template 3</a>
                            <a class="dropdown-item" href="index-4.html">Demo Template 4</a>
                            <a class="dropdown-item" href="index-5.html">Demo Template 5</a>
                            <a class="dropdown-item" href="index-6.html">Demo Template 6</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#features">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#pricing">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#screenshots">Screenshots</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact">Contact</a>
                    </li>

                </ul>
            </div> -->
        {{-- </div>
    </nav> --}}
    <!--end navbar-->
</header>
<!--header section end-->

<!--body content wrap start-->
<div class="main">

    <!--hero section start-->
    <section class="hero-section hero-section-3 ptb-100">
        <!--animated circle shape start-->
        <div class="circles">
            <div class="point animated-point-1"></div>
            <div class="point animated-point-2"></div>
            <div class="point animated-point-3"></div>
            <div class="point animated-point-4"></div>
            <div class="point animated-point-5"></div>
            <div class="point animated-point-6"></div>
        </div>
        <!--animated circle shape end-->
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-6">
                    <div class="hero-content-left ptb-100">
                        <h1><span style="color:rgb(0, 62, 82)">You’re interested, AWESOME.</span></h1>
                        <br>
                        <p class="lead" style="font-size:26px;">Here’s the full agenda of the Real Estate Study Tour.</p>
                        <a href="#contact" class="btn solid-btn page-scroll" style="width:200px;"><span style="font-size:25px;">Register</span></a>
                        {{-- <br>
                        <a href="#contact" class="btn solid-btn page-scroll btn-attend"><span style="font-size:25px;">Get Started</span><br>Here's the full agenda.</a>
                        <a href="#contact" class="btn solid-btn page-scroll" style="margin-left:10px;"><span style="font-size:25px;">Interested</span><br>But can't attend the tour?</a> --}}
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="hero-animation-img">
                        <img class="img-fluid d-block m-auto animation-two" src="{{ URL::asset('new-assets/img/awesome.png')}}"
                             width="470" alt="animation image">
                        {{-- <img class="img-fluid d-none d-lg-block animation-two"
                             src="{{ URL::asset('new-assets/img/header-slider-img-2.png')}}" alt="animation image" width="580"> --}}
                        <!-- <img class="img-fluid d-none d-lg-block animation-three"
                             src="img/Telemedicine_03.svg" alt="animation image" width="120">
                        <img class="img-fluid d-none d-lg-block animation-four" src="img/hero-animation-03.svg"
                             alt="animation image" width="200"> -->
                    </div>
                </div>
            </div>
        </div>

        <!--shape image start-->
        <img src="{{ URL::asset('new-assets/img/hero-bg-shape-2.png')}}" class="shape-image" alt="shape image">
        <!--shape image end-->
    </section>
    <!--hero section end-->



    <!--overflow block start-->
    <div class="overflow-hidden">
        <!--about us section start-->
        <section id="about" class="about-us ptb-100 background-shape-img">
            <div class="container" style="z-index:3">
                <div class="row align-items-center justify-content-between">
                    <div class="col-md-12">
                        <div class="about-content-left section-heading">
                            {{-- <h2 style="color:#fff;padding-bottom:50px;">Agenda</h2> --}}
                            <div class="row" style="color:#fff;font-size: 17px;">
                                    <div class="col-md-6">
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap d-flex align-items-center mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Workshop with Offsure Co-Founder Jason Rechenberg about the benefits of working with an offshore Real Estate team like grow your rent roll, grow your acquisitions, improve client interface and more.</p>
                                                </div>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Focus group with Offsure’s Real Estate team about outsourcing more than 20 property management, marketing, sales administration and lead generation tasks.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Real Estate business workshop with Jason to determine your goals, strategy and people plan for 2020.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">KPI (performance indicator) discussion with Jason to determine what’s working and what’s not working within your Real Estate business model.</p>
                                                </div>
                                                <p></p>
                                            </div>

                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">See Property Management virtual assistants and lead generation specialists in action on how they provide more focused time to principals, property managers and agents.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                    </div>

                                    <div class="col-md-6">
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Observe Offsure’s Real Estate team as they utilise Real Estate software and programs like PropertyMe, PropertyTree, Rest, Console, IRE (Inspect Real Estate) and more. As well as using database checks like NTD (National Tenancy Database), TICA and Barclays.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Meet and interview potential professionals with years of experience in the Real Estate industry as property management virtual or admin assistants. We’ve got a shortlist at the ready to join your competitive team.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Immerse yourself in the workforce here in OFFSURE to learn more about the Filipino work culture.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="single-feature mb-4">
                                                <div class="icon-box-wrap mb-2">
                                                    <div class="mr-3 icon-box">
                                                        <!-- <img src="img/image-icon-1.png" alt="icon image" class="img-fluid"> -->
                                                    </div>
                                                    <p class="mb-0">Meet the entire Offsure team working with businesses in Australia and New Zealand engaged in different professions and industries.</p>
                                                </div>
                                                <p></p>
                                            </div>
                                    </div>



                            </div>
                            <div class="row mt-4">
                                <div class="col-lg-12 text-center">
                                    <a href="#contact" class="btn solid-btn page-scroll btn-second" style="width:200px;"><span style="font-size:25px;">Register</span></a>
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- <div class="col-md-5">
                        <div class="about-content-right d-flex justify-content-center justify-content-lg-end justify-content-md-end">
                            <img src="img/image-10.png" alt="about us" class="img-fluid">
                        </div>
                    </div> -->


                </div>
            </div>
        </section>
        <!--about us section end-->


    </div>
    <!--overflow block end-->


    <section id="inclusions" class="contact-us white-bg ptb-100" style="padding-bottom:50px;">
        <div class="container">
            <div class="row">

                <div class="col-md-12" >
                    <h2>Tour inclusions:</h2>
                    <ul>
                        <li>3 nights hotel accommodation including breakfasts at the 5-star Midori Hotel and Casino.</li>
                        <li>Lunches and dinners</li>
                        <li>Private airport/office transport</li>

                    </ul>
                    <br><br>
                    <h2>For only <span style="color:#FF4813;font-size:60px;">AUD 990</span></h2>
                </div>
            </div>
        </div>
    </section>





    <!--contact us section start-->
    <section id="contact" class="contact-us gray-light-bg ptb-100">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <form action="#" method="POST" id="contactForm1" class="contact-us-form" novalidate="novalidate">
                        <h5>You can purchase your Study Tour ticket below by filling out the necessary information to reserve your ticket for April. We’ll see you there.</h5>
                        <br>


                        {{-- <div class="row">
                            <div class="col-sm-12 mt-3">
                                <button type="submit" class="btn solid-btn" id="btnContactUs">
                                    Submit
                                </button>
                            </div>
                        </div> --}}
                    </form>
                    <p class="form-message"></p>
                </div>


                <div class="col-md-12">
                        <div class="panel panel-default credit-card-box">
                            <div class="panel-heading display-table" >
                                <div class="row display-tr" >
                                    <h3 class="panel-title display-td" >Personal and Payment Details</h3>
                                    <div class="display-td" >
                                        <img class="img-responsive pull-right" src="{{ URL::asset('new-assets/img/accepted_c22e0.png')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">

                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <p style="color:">{{ Session::get('success') }}</p>
                                    </div>
                                @endif

                                <form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
                                                                    data-cc-on-file="false"
                                                                data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                                id="payment-form">
                                    @csrf


                                    <div class="row">
                                        <div class="col-sm-6 col-12">
                                            <div class="form-grouprequired">
                                                <input type="text" class="form-control name" name="name" placeholder="Enter name"
                                                        required="required">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group required">
                                                <input type="email" class="form-control email" name="email" placeholder="Enter email"
                                                        required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group required">
                                                <input type="text" name="phone" value="" class="form-control phone" id="phone"
                                                        placeholder="Your Phone" required="required">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group required">
                                                <input type="text" name="company" value="" size="40" class="form-control company"
                                                        id="company" placeholder="Your Company">
                                            </div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class='form-row row'>
                                        <div class='col-xs-12 form-group required'>
                                            <label class='control-label'>Name on Card</label> <input
                                                class='form-control' size='4' type='text'>
                                        </div>
                                    </div>

                                    <div class='form-row row'>
                                        <div class='col-xs-12 form-group card required'>
                                            <label class='control-label'>Card Number</label> <input
                                                autocomplete='off' class='form-control card-number' size='20'
                                                type='text'>
                                                <input name="card_number" style="display:none;" class="card-number1">
                                        </div>
                                    </div>

                                    <div class='form-row row'>
                                        <div class='col-xs-12 col-md-4 form-group cvc required'>
                                            <label class='control-label'>CVC</label> <input autocomplete='off'
                                                class='form-control card-cvc' placeholder='ex. 311' size='4'
                                                type='text'>
                                        </div>
                                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                                            <label class='control-label'>Expiration Month</label> <input
                                                class='form-control card-expiry-month' placeholder='MM' size='2'
                                                type='text'>
                                        </div>
                                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                                            <label class='control-label'>Expiration Year</label> <input
                                                class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                                type='text'>
                                        </div>
                                    </div>

                                    <div class='form-row row'>
                                        <div class='col-md-12 error form-group hide'>
                                            <div class='alert-danger alert'>Please correct the errors and try
                                                again.</div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
            </div>

        </div>
    </section>
    <!--contact us section end-->



    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg pt-4 pb-4">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-12"><p class="copyright-text pb-0 mb-0">Copyrights © 2019. All
                    rights reserved by
                    <a href="https://offsure.com">Offsure</a></p>
                </div>
            </div>
            <div class="row text-center justify-content-center">
                <img src="{{ URL::asset('new-assets/img/stripelogo.png')}}" width="200px"/>

            </div>
        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--jQuery-->
{{-- <script src="{{ URL::asset('new-assets/js/jquery-3.4.1.min.js')}}"></script> --}}
<!--Popper js-->
<script src="{{ URL::asset('template-assets/scripts/jquery-1.8.2.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('new-assets/js/popper.min.js')}}"></script>
<!--Bootstrap js-->
<script src="{{ URL::asset('new-assets/js/bootstrap.min.js')}}"></script>
<!--Magnific popup js-->
<script src="{{ URL::asset('new-assets/js/jquery.magnific-popup.min.js')}}"></script>
<!--jquery easing js-->
<script src="{{ URL::asset('new-assets/js/jquery.easing.min.js')}}"></script>
<!--jquery ytplayer js-->
<script src="{{ URL::asset('new-assets/js/jquery.mb.YTPlayer.min.js')}}"></script>
<!--wow js-->
<script src="{{ URL::asset('new-assets/js/wow.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{ URL::asset('new-assets/js/owl.carousel.min.js')}}"></script>


<script src="{{ URL::asset('template-assets/scripts/jquery.validate.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/smoothscroll.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/jquery.superslides.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/placeholders.jquery.min.js')}}"></script>
<!--custom js-->
<script src="{{ URL::asset('new-assets/js/scripts.js')}}"></script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');

        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });

    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }

  });

  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }

});
</script>
</body>
</html>
