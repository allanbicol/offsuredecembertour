<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="Offsure Referral Program">
    <meta name="author" content="Offsure">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="Offsure Referral Program"/> <!-- website name -->
    <meta property="og:site" content="https://referral.offsure.com/referral-program/refer-a-friend"/> <!-- website link -->
    <meta property="og:title" content="Offsure Referral Program"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Refer someone, Get paid P2,000"/> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="{{ URL::asset('new-assets/img/fb_thumbnail.jpg')}}"/> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="https://referral.offsure.com/referral-program/refer-a-friend"/> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article"/>

    <!--title-->
    <title>Offsure Referral Program</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/bootstrap.min.css')}}">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/magnific-popup.css')}}">
    <!--Themify icon css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/themify-icons.css')}}">
    <!--animated css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/animate.min.css')}}">
    <!--ytplayer css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/jquery.mb.YTPlayer.min.css')}}">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.theme.default.min.css')}}">
    <!--custom css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/responsive.css')}}">

</head>
<body>

<!--header section start-->


<!--body content wrap start-->
<div class="main">
    <div class="container" style="text-align:center;padding-top:15px;background:#f4f7fa">
            <div class="row">
                <div class="col-md-12">
                        <a class="navbar-brand" href="#"><img src="{{ URL::asset('new-assets/img/logo1.png')}}" width="170" alt="logo"
                            class="img-fluid"></a>
                </div>
            </div>
            <div class="row" style="padding-top:50px;color:rgb(0, 62, 82);padding-left:20px;padding-right:20px;">
                <div class="col-md-12">
                        <span style="font-size:20px;font-weight:bolder;margin-left:-55px;">Refer someone, </span><span style="position:relative; "> <img src="{{ URL::asset('new-assets/img/megaphone.png')}}" style="margin-top:-5px;position:absolute;left:5px;" width="40px;"></span>
                </div>
                <div class="col-md-12" style="border-top:2px solid #003E52;border-bottom:2px solid #003E52;padding-top:15px;">
                        <h1 style="color:#003E52"><b>get paid</b> Php2,000</h1>
                </div>
            </div>
    </div>

    <!--contact us section start-->
    <section id="contact" class="contact-us gray-light-bg ptb-100 text-center">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    @if($message = Session::get('success'))
                        <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>{{ $message }}
                        </div>
                    @endif
                    <form action="{{route('add.referral')}}" method="POST" id="contactForm1" class="contact-us-form" >
                            {{ csrf_field() }}
                        <h4 style="color:#FF4813">Fill up the details below.</h4>
                        <br>
                        <h5>Your Details</h5>
                        <div class="row">
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control text-center" name="name" placeholder="Your name"
                                           required="required">
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="email" class="form-control text-center" name="email" placeholder="Your email"
                                           required="required">
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control text-center" name="mobile" placeholder="Your mobile number"
                                           required="required">
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <h5>Your Referral’s Details</h5>
                        <div class="row ref-details">
                            <div class="col-sm-12 col-12" >
                                <div class="row" style="border-bottom:1px dashed #ffad95;padding-top:5px;">
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control text-center" name="ref_name[]" placeholder="Enter name"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control text-center" name="ref_email[]" placeholder="Enter email"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control text-center" name="ref_mobile[]" placeholder="Enter mobile number"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-12">
                                        <div class="form-group">
                                            <select class="browser-default custom-select" name="ref_target_job[]" required>
                                                <option selected value="">Select target position</option>
                                                @foreach ($job_openings as $job_opening )
                                                    <option value="{{$job_opening->job_title}}">{{$job_opening->job_title}}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" class="form-control text-center" name="ref_target_job[]" placeholder="Enter target job role" required="required"> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mt-3">
                                <a  class="btn-add-new" id="btnContactUs" style="cursor:pointer;color:#FF4813;text-decoration:underline;font-size:15px;">
                                    Add another referral
                                </a>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-12 mt-3">
                                <button type="submit" class="btn solid-btn btn-attend" id="btnContactUs" style="font-size:23px">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                    <p class="form-message"></p>
                </div>
            </div>
        </div>
    </section>
    <!--contact us section end-->



    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg pt-4 pb-4">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-6 col-lg-5"><p class="copyright-text pb-0 mb-0">Copyrights © 2019. All
                    rights reserved by
                    <a href="https://offsure.com">Offsure</a></p>
                </div>
            </div>


        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--jQuery-->
<script src="{{ URL::asset('new-assets/js/jquery-3.4.1.min.js')}}"></script>
<!--Popper js-->
<script src="{{ URL::asset('new-assets/js/popper.min.js')}}"></script>
<!--Bootstrap js-->
<script src="{{ URL::asset('new-assets/js/bootstrap.min.js')}}"></script>
<!--Magnific popup js-->
<script src="{{ URL::asset('new-assets/js/jquery.magnific-popup.min.js')}}"></script>
<!--jquery easing js-->
<script src="{{ URL::asset('new-assets/js/jquery.easing.min.js')}}"></script>
<!--jquery ytplayer js-->
<script src="{{ URL::asset('new-assets/js/jquery.mb.YTPlayer.min.js')}}"></script>
<!--wow js-->
<script src="{{ URL::asset('new-assets/js/wow.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{ URL::asset('new-assets/js/owl.carousel.min.js')}}"></script>
<!--custom js-->
<script src="{{ URL::asset('new-assets/js/scripts.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121809826-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-121809826-1');
</script>
<script>
        $(".btn-add-new").on("click", function(event) {
               $('.ref-details').append('<div class="col-sm-12 col-12">'+
                        '<div class="row" style="border-bottom:1px dashed #ffad95;padding-top:5px;">'+
                        '<div class="col-sm-4 col-12">'+
                            '<div class="form-group">'+
                                '<input type="text" class="form-control text-center" name="ref_name[]" placeholder="Enter name"'+
                                '       required="required">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-4 col-12">'+
                            '<div class="form-group">'+
                                '<input type="email" class="form-control text-center" name="ref_email[]" placeholder="Enter email"'+
                                        'required="required">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-4 col-12">'+
                            '<div class="form-group">'+
                                '<input type="text" class="form-control text-center" name="ref_mobile[]" placeholder="Enter mobile number"'+
                                        'required="required">'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12 col-12">'+
                            '<div class="form-group">'+
                            '<select class="browser-default custom-select" name="ref_target_job[]" required>'+
                                '<option selected value="">Select target position</option>'+
                                @foreach ($job_openings as $job_opening )
                                '   <option value="{{$job_opening->job_title}}">{{$job_opening->job_title}}</option>'+
                                @endforeach
                            '</select>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
               )
            });

        </script>
</body>
</html>
