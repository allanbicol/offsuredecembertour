<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta description -->
    <meta name="description"
          content="Offsure Referral Program">
    <meta name="author" content="Offsure">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="Offsure Referral Program"/> <!-- website name -->
    <meta property="og:site" content="https://referral.offsure.com/referral-program/refer-a-friend"/> <!-- website link -->
    <meta property="og:title" content="Offsure Referral Program"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Refer someone, Get paid P2,000"/> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="{{ URL::asset('new-assets/img/fb_thumbnail.jpg')}}"/> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="https://referral.offsure.com/referral-program/refer-a-friend"/> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article"/>

    <!--title-->
    <title>Offsure Referral Program</title>

    <!--favicon icon-->
    <link rel="icon" href="img/favicon.png" type="image/png" sizes="16x16">

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700%7COpen+Sans&display=swap"
          rel="stylesheet">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/bootstrap.min.css')}}">
    <!--Magnific popup css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/magnific-popup.css')}}">
    <!--Themify icon css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/themify-icons.css')}}">
    <!--animated css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/animate.min.css')}}">
    <!--ytplayer css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/jquery.mb.YTPlayer.min.css')}}">
    <!--Owl carousel css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/owl.theme.default.min.css')}}">
    <!--custom css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/style.css')}}">
    <!--responsive css-->
    <link rel="stylesheet" href="{{ URL::asset('new-assets/css/responsive.css')}}">

</head>
<body>

<!--header section start-->
<header class="header">
    <!--start navbar-->
    <nav class="navbar navbar-expand-lg fixed-top custom-nav white-bg" style="padding:30px;">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="{{ URL::asset('new-assets/img/logo1.png')}}" width="200" alt="logo"
                                                           class="img-fluid"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button>

            <!-- <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link page-scroll dropdown-toggle" href="#" id="navbarDropdownHome" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Home
                        </a>
                        <div class="dropdown-menu submenu" aria-labelledby="navbarDropdownHome">
                            <a class="dropdown-item" href="index.html">Demo Template 1</a>
                            <a class="dropdown-item" href="index-2.html">Demo Template 2</a>
                            <a class="dropdown-item" href="index-3.html">Demo Template 3</a>
                            <a class="dropdown-item" href="index-4.html">Demo Template 4</a>
                            <a class="dropdown-item" href="index-5.html">Demo Template 5</a>
                            <a class="dropdown-item" href="index-6.html">Demo Template 6</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#features">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#pricing">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#screenshots">Screenshots</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact">Contact</a>
                    </li>

                </ul>
            </div> -->
        </div>
    </nav>
    <!--end navbar-->
</header>
<!--header section end-->

<!--body content wrap start-->
<div class="main">

    <!--hero section start-->
    <section class="hero-section hero-section-3 ptb-100">
        <!--animated circle shape start-->
        <div class="circles">
            <div class="point animated-point-1"></div>
            <div class="point animated-point-2"></div>
            <div class="point animated-point-3"></div>
            <div class="point animated-point-4"></div>
            <div class="point animated-point-5"></div>
            <div class="point animated-point-6"></div>
        </div>
        <!--animated circle shape end-->
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-6">
                    <div class="hero-content-left ptb-100">
                        <h1 class="hero-h1"><span style="color:rgb(0, 62, 82)">Refer someone,</span><div style="margin-top:-10px">Get paid P2,000</div></h1>
                        {{-- <div style="margin-top:-20px;color:#FF4813;font-weight: bold;">December 10 - 11 | Clark, Philippines</div> --}}
                        {{-- <div style="color:rgb(0, 62, 82)">Exclusive to owners, brokers and principals in AU and NZ.</div> --}}
                        {{-- <br>
                        <p class="lead">What is your Real Estate plan for 2020? If you haven’t <br>figured it out, maybe we can help.</p> --}}
                        <br>
                        <a href="#contact" class="btn solid-btn page-scroll btn-attend"><span style="font-size:25px;">Refer Now</span></a>

                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="hero-animation-img">
                        <img class="img-fluid d-block m-auto animation-one_a" src="{{ URL::asset('new-assets/img/referral-guy1.png')}}"
                             width="400" alt="animation image">
                        <img class="img-fluid d-none d-lg-block animation-two_a"
                             src="{{ URL::asset('new-assets/img/tell-a-friend.png')}}" alt="animation image" width="200">
                        <!-- <img class="img-fluid d-none d-lg-block animation-three"
                             src="img/Telemedicine_03.svg" alt="animation image" width="120">
                        <img class="img-fluid d-none d-lg-block animation-four" src="img/hero-animation-03.svg"
                             alt="animation image" width="200"> -->
                    </div>
                </div>
            </div>
        </div>

        <!--shape image start-->
        <img src="{{ URL::asset('new-assets/img/hero-bg-shape-2.png')}}" class="shape-image" alt="shape image">
        <!--shape image end-->
    </section>
    <!--hero section end-->






    <!--contact us section start-->
    <section id="contact" class="contact-us gray-light-bg ptb-100">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    @if($message = Session::get('success'))
                        <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>{{ $message }}
                        </div>
                    @endif
                    <form action="{{route('add.referral')}}" method="POST" id="contactForm1" class="contact-us-form" >
                            {{ csrf_field() }}
                        <h4>Fill up the details below.</h4>
                        <br>
                        <h5>Your Details</h5>
                        <div class="row">
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Your name"
                                           required="required">
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Your email"
                                           required="required">
                                </div>
                            </div>
                            <div class="col-sm-4 col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="mobile" placeholder="Your mobile number"
                                           required="required">
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <h5>Your Referral’s Details</h5>
                        <div class="row ref-details">
                            <div class="col-sm-12 col-12" >
                                <div class="row" style="border-bottom:1px dashed #ffad95;padding-top:5px;">
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="ref_name[]" placeholder="Enter name"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="ref_email[]" placeholder="Enter email"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="ref_mobile[]" placeholder="Enter mobile number"
                                                required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-12">
                                        <div class="form-group">
                                            <select class="browser-default custom-select" name="ref_target_job[]" required>
                                                <option selected value="">Select target position</option>
                                                @foreach ($job_openings as $job_opening )
                                                    <option value="{{$job_opening->job_title}}">{{$job_opening->job_title}}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" class="form-control" name="ref_target_job[]" placeholder="Enter target job role" required="required"> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mt-3">
                                <button type="button" class="btn solid-btn btn-add-new" id="btnContactUs">
                                    Add another referral
                                </button>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-sm-12 mt-3">
                                <button type="submit" class="btn solid-btn btn-attend" id="btnContactUs" style="font-size:23px">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                    <p class="form-message"></p>
                </div>
            </div>
        </div>
    </section>
    <!--contact us section end-->



    <!--footer copyright start-->
    <div class="footer-bottom gray-light-bg pt-4 pb-4">
        <div class="container">
            <div class="row text-center justify-content-center">
                <div class="col-md-6 col-lg-5"><p class="copyright-text pb-0 mb-0">Copyrights © 2019. All
                    rights reserved by
                    <a href="https://offsure.com">Offsure</a></p>
                </div>
            </div>


        </div>
    </div>
    <!--footer copyright end-->
</footer>
<!--footer section end-->

<!--jQuery-->
<script src="{{ URL::asset('new-assets/js/jquery-3.4.1.min.js')}}"></script>
<!--Popper js-->
<script src="{{ URL::asset('new-assets/js/popper.min.js')}}"></script>
<!--Bootstrap js-->
<script src="{{ URL::asset('new-assets/js/bootstrap.min.js')}}"></script>
<!--Magnific popup js-->
<script src="{{ URL::asset('new-assets/js/jquery.magnific-popup.min.js')}}"></script>
<!--jquery easing js-->
<script src="{{ URL::asset('new-assets/js/jquery.easing.min.js')}}"></script>
<!--jquery ytplayer js-->
<script src="{{ URL::asset('new-assets/js/jquery.mb.YTPlayer.min.js')}}"></script>
<!--wow js-->
<script src="{{ URL::asset('new-assets/js/wow.min.js')}}"></script>
<!--owl carousel js-->
<script src="{{ URL::asset('new-assets/js/owl.carousel.min.js')}}"></script>
<!--custom js-->
<script src="{{ URL::asset('new-assets/js/scripts.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121809826-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-121809826-1');
</script>
<script>
$(".btn-add-new").on("click", function(event) {
       $('.ref-details').append('<div class="col-sm-12 col-12">'+
                '<div class="row" style="border-bottom:1px dashed #ffad95;padding-top:5px;">'+
                '<div class="col-sm-4 col-12">'+
                    '<div class="form-group">'+
                        '<input type="text" class="form-control" name="ref_name[]" placeholder="Enter name"'+
                        '       required="required">'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-4 col-12">'+
                    '<div class="form-group">'+
                        '<input type="email" class="form-control" name="ref_email[]" placeholder="Enter email"'+
                                'required="required">'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-4 col-12">'+
                    '<div class="form-group">'+
                        '<input type="text" class="form-control" name="ref_mobile[]" placeholder="Enter mobile number"'+
                                'required="required">'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-12 col-12">'+
                    '<div class="form-group">'+
                    '<select class="browser-default custom-select" name="ref_target_job[]" required>'+
                        '<option selected value="">Select target position</option>'+
                        @foreach ($job_openings as $job_opening )
                        '   <option value="{{$job_opening->job_title}}">{{$job_opening->job_title}}</option>'+
                        @endforeach
                    '</select>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
       )
    });

</script>
</body>
</html>
