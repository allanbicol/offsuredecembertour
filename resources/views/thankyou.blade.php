<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>Duos - Multipurpose HTML5 Landing Page</title>
<meta name="description" content="Startup landing page">
<meta name="author" content="Multifour | multifour.com">

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->
<link rel="stylesheet" href="{{ URL::asset('template-assets/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{ URL::asset('template-assets/css/style.css')}}"/>
<link rel="stylesheet" href="{{ URL::asset('template-assets/css/iconfont-style.css')}}"/>

<!-- Favicons -->
<link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
</head>

<body>
<div id="wrap">
	<div class="container-fluid">
		<div class="row">

			<!-- INTRO BEGIN -->
			<div id="intro" class="col-lg-6"> <a href="#" class="logo">
                 {{-- <img src="{{ URL::asset('template-assets/images/studytour.png')}}" alt="Duos - Multipurpose HTML5 Landing Page" width="250"/>  --}}
                </a>

				<!-- NAVIGATION BEGIN -->
				{{-- <nav class="navbar"> <a class="menu-btn collapsed" data-toggle="collapse" href="#menu-list"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
					<div id="menu-list" class="panel-collapse collapse">
						<ul class="nav">
							<li><a href="about.html">About</a> </li>
							<li><a href="contact.html">Contact</a></li>
							<li><a href="http://themeforest.net/item/duos-multipurpose-html5-landing-page/9532968?ref=multifour" target="_blank">Sign In</a></li>
						</ul>
					</div>
				</nav> --}}
				<!-- NAVIGAION END -->

				<div id="slides">
					<ul class="slides-container_" >
						<li class="orange-bg">
							<h2 class="slogan"> <img src="{{ URL::asset('template-assets/images/studytour.png')}}" style="opacity:1;" width="80%"/></h2>
							<img src="{{ URL::asset('template-assets/images/1.jpg')}}" alt=""> </li>
						{{-- <li class="blue-bg">
							<h2 class="slogan"> Simple &amp; Pure <small>No fluff. Nothing should lead the visitor away from the main essence of website. There must be just important information.</small> </h2>
							<img src="images/2.jpg" alt=""> </li>
						<li class="purple-bg">
							<h2 class="slogan"> Innovations <small>In our work we try to use only the most modern, convenient and interesting solutions.
								We want the template you downloaded look unique and new for such a long time as it is possible.</small> </h2>
							<img src="images/3.jpg" alt=""> </li> --}}
					</ul>
				</div>
			</div>
			<!-- INTRO END -->

			<!-- CONTENT BEGIN -->
			<div id="content" class="main-page col-lg-6 col-lg-offset-6">
                <img src="{{ URL::asset('template-assets/images/smile.png')}}" width="50px"/>
				<div style="font-size:45px;margin-top:-30px;">Thank you!</div>
                <div style="font-size:45px;margin-top:-50px;">Your payment was successful.</div>
                <div style="font-size:24px;margin-top:-30px;color:#FF4813">A purchase confirmation was sent to your email address. Thank you and see you there.</div>
                <a class="btn btn-primary" href="{{route('home')}}">Ok!</a>
            </div>
			<!-- CONTENT END -->

		</div>
	</div>
</div>



<!-- JavaScript -->
<script src="{{ URL::asset('template-assets/scripts/jquery-1.8.2.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/jquery.validate.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/smoothscroll.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/jquery.superslides.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/placeholders.jquery.min.js')}}"></script>
<script src="{{ URL::asset('template-assets/scripts/custom.js')}}"></script>

<!--[if lte IE 9]>
	<script src="{{ URL::asset('template-assetscss/scripts/respond.min.js')}}"></script>
<![endif]-->
</body>
</html>
