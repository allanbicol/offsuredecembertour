<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StripePaymentController@index')->name('home');
Route::get('/signup', 'StripePaymentController@home')->name('signup');
Route::get('/success', 'StripePaymentController@thankyou')->name('thankyou');
Route::get('/prospect-success', 'StripePaymentController@thankyou1')->name('thankyou1');
Route::get('/payment', 'StripePaymentController@stripe')->name('stripe.form');
Route::post('/stripe', 'StripePaymentController@stripePost')->name('stripe.post');
Route::post('/add-customer-stripe', 'StripePaymentController@stripeAddCustomer')->name('stripe.add');
Route::post('/add-prospect', 'StripePaymentController@addProspect')->name('add.prospect');

Route::get('/referral-program/refer-a-friend', 'ReferralController@index')->name('refer-a-friend');
Route::post('/referral-program/add', 'ReferralController@addReferral')->name('add.referral');
