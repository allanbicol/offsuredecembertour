<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referree extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_referree';



    public $timestamps = false;
}
