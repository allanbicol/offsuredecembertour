<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Referree;
use App\Referral;
use Mobile_Detect;
use App\JobOpening;

class ReferralController extends Controller
{
    public function index(){
        $detect = new Mobile_Detect;

        $jobOpenings = JobOpening::orderBy('id','asc')
                            ->get();
        if($detect->isMobile()=='mobile'){
            return view('refer.referafriend-mobile',['job_openings'=>$jobOpenings]);
        }else{
            return view('refer.referafriend',['job_openings'=>$jobOpenings]);
        }

    }

    public function addReferral(Request $request){
        $this->validate($request, [
            'name' =>'required',
            'email'=>'email|required',
            'mobile'=>'required',
        ]);

        $referree = new Referree();
        $referree->name = $request->name;
        $referree->email = $request->email;
        $referree->mobile = $request->mobile;
        $referree->date_added = Carbon::now();
        $referree->save();

        $referreeId = $referree->id;
        // echo $referreeId;
        // print_r($request->ref_name[0]);
        // exit();

        for($i=0; $i < count($request->ref_name); $i++) {
            $referral = new Referral();
            $referral->referree_id = $referreeId;
            $referral->name = $request->ref_name[$i];
            $referral->email = $request->ref_email[$i];
            $referral->mobile = $request->ref_mobile[$i];
            $referral->target_position = $request->ref_target_job[$i];
            $referral->save();
        }

        return back()->with('success','Thank you. We\'ll get in touch with your referrals on your behalf and we\'ll contact you if they got the job so you can get your P2,000.');
    }
}
