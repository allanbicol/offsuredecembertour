<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Stripe;

class StripePaymentController extends UtilApiController
{
    public function index()
    {
        return view('index');
    }

    public function home()
    {
        return view('signup');
    }

    public function thankyou()
    {
        return view('thankyou');
    }

    public function thankyou1()
    {
        return view('thankyou1');
    }



    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        $data1 = array(array(
            'email'=>$request->email,
            'full_name'=>$request->name,
            'phone'=>$request->phone,
            'company'=>$request->company,
            'card_name'=>$request->input('event')
        ));
        // $list = new UtilAPIController;

        $result = $this->addAttendee($data1);

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 990 * 100,
                "currency" => "AUD",
                "source" => $request->stripeToken,
                "description" => "Offsure Real Estate Study Tour",
                'receipt_email' => $request->email,

        ]);

        // Session::flash('success', 'Payment successful!');
        return redirect('/success');

        return back();
    }

    public function stripeAddCustomer(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Customer::create ([
            'email' => $request->email,
            'payment_method' => 'ch_1FauCwC2wQBA6grvcy6JtCs9',
            'invoice_settings' => [
              'default_payment_method' => 'ch_1FauCwC2wQBA6grvcy6JtCs9',
            ],
        ]);
    }

    public function addProspect(Request $request)
    {
        $preferred_option = $request->phone_option.' '. $request->email_option;
        $data1 = array(array(
            'email'=>$request->email,
            'full_name'=>$request->name,
            'phone'=>$request->phone,
            'company'=>$request->company,
            'preferred_date'=>$request->date,
            'preferred_time'=>$request->time,
            'preferred_communication'=>$preferred_option,
        ));

        $result = $this->addProspects($data1);

        // Session::flash('success', 'Payment successful!');
        return redirect('/prospect-success');

    }

}
