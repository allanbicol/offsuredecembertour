<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_referral';



    public $timestamps = false;
}
