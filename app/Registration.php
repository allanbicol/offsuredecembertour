<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_registration';
}
