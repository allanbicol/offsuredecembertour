<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOpening extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_job_opening';



    public $timestamps = false;
}
